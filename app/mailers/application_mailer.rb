class ApplicationMailer < ActionMailer::Base
  default from: "chrissywong1303@gmail.com"
  layout 'mailer'
end
